#! /bin/bash
##Если свалится одна из команд, рухнет и весь скрипт
set -xe
#Перезаливаем дескриптор сервиса на ВМ для деплоя
sudo cp -rf sausage-store-frontend.service /etc/systemd/system/sausage-store-frontend.service  
sudo rm -f /var/www-data/dist/frontend/*||true
#Переносим артефакт в нужную папку
curl -H "Authorization: Basic ${NEXUS_AUTH}"  -o sausage-store-${VERSION}.tar.gz   ${NEXUS_REPO_FRONTEND_URL}/sausage-store-front/sausage-store/${VERSION}/sausage-store-${VERSION}.tar.gz
tar xvf sausage-store-${VERSION}.tar.gz
sudo mv sausage-store-${VERSION}/public_html/* /var/www-data/dist/frontend/
sudo rm sausage-store-${VERSION}.tar.gz
rm -R sausage-store-${VERSION}
#sudo cp ./sausage-store.jar /home/jarservice/sausage-store.jar||true #"jar||true" говорит, если команда обвалится — продолжай
#Обновляем конфиг systemd с помощью рестарта
sudo systemctl daemon-reload
#Перезапускаем сервис сосисочной   
sudo systemctl restart  sausage-store-frontend.service  
